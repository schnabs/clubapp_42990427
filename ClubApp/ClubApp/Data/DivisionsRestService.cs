﻿using ClubApp.Models;
using MvvmHelpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ClubApp.Data
{
    class DivisionsRestService
    {
        HttpClient client;


        public DivisionsRestService()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
        }

        public async Task<ObservableRangeCollection<Division>> GetDivisionsAsync(DemoEntity league)
        {
            //TODO pass in entity value 

            var Divisions = new ObservableRangeCollection<Division>();

            for (int i = 0; i < 100; i++)
            {
                Divisions.Add(new Division { Name = $"D {i}", City = $"C {i}", Province = $"P {i}", AgeGroupName = $"AG {i}", LeagueName = $"LN {i}" });
            }

            await Task.Delay(500);

            /*
            var uri = new Uri(Constants.BaseUrl + $"/json/getDivisions.php?entity_value={league.ID}&entity_id={league.EntityID}");

            var response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                try
                {

                    var content = await response.Content.ReadAsStringAsync();

                    var divisions = JsonConvert.DeserializeObject<List<Division>>(content);
                    Debug.WriteLine($"Server Returned {divisions.Count} divisions");                   

                    /*PD - not required to filter since we are sending in entity ID and value to the JSON file now
                    var filtered = from div in divisions
                                   where div.LeagueID == league.ID
                                   select div;
                    */
                    /*
                    Divisions.ReplaceRange(divisions);

                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
            }
            */

            return Divisions;
        }



    }
}
