﻿using ClubApp.Models;
using MvvmHelpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ClubApp.Data
{
    class LeagueRestService
    {
        HttpClient client;


        public LeagueRestService()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
        }

        public async Task<ObservableRangeCollection<League>> GetLeaguesAsync()
        {
            var leagues = new ObservableRangeCollection<League>();

            for(int i =0; i<100; i++)
            {
                leagues.Add(new League() { Name = $"L {i}", City = $"P {i}", Province = $"P {i}" });
            }

            await Task.Delay(2 * 1000);

            /*
            var uri = new Uri(Constants.BaseUrl + $"/json/getEntitiesForAssociation.php?aid={Constants.AssociationID}&entity_id={(int)DemoEntities.League}");
            
            var response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                try
                {

                    var content = await response.Content.ReadAsStringAsync();

                    var leaguesFromJson = JsonConvert.DeserializeObject<List<League>>(content);
                    Debug.WriteLine($"Server Returned {leaguesFromJson.Count} leagues");

                    leagues.AddRange(leaguesFromJson);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
            }
            */

            return leagues;
        }
    }
}
