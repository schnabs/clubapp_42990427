﻿using ClubApp.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace ClubApp
{
    public class App : Application
    {
        public App()
        {
            /*MainPage = new NavigationPage(
                //new SplashPage()
                //new PoolRankingsPage(76)
                //new DivisionsPage()
                new LeaguesMDPage()
                //new LeaguesPage()
                );*/
            MainPage = new MainMasterDetailPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
