﻿using MvvmHelpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClubApp.Models
{
    public class Division : DemoEntity
    {
        public Division()
        {
            base.EntityID = (int)ClubEntities.Division;
        }

        [JsonProperty(PropertyName = "division_id")]
        public int DivisionID {
            get { return base.ID; }
            set { base.ID = value; }
        }

        [JsonProperty(PropertyName = "division_name")]
        public string DivisionName { 
            get { return base.Name; }
            set { base.Name = value; }
        }

        [JsonProperty(PropertyName = "agegroup_id")]
        public int AgeGroupID { get; set; }

        [JsonProperty(PropertyName = "age_group")]
        public string AgeGroupName { get; set; }

        [JsonProperty(PropertyName = "league_id")]
        public int LeagueID { get; set; }

        [JsonProperty(PropertyName = "league_name")]
        public string LeagueName { get; set; }

        [JsonProperty(PropertyName = "gender")]
        public string Gender { get; set; }

    }

}
