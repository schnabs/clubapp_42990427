﻿using MvvmHelpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ClubApp.Models
{
    public class DemoEntity : ObservableObject
    {
        public int ID { get; set; }

        [JsonProperty(PropertyName = "entity_id")]
        public int EntityID { get; set; }

        [JsonProperty(PropertyName = "entity_value")]
        public int EntityValue {
            get { return ID; }
            set { ID = value; }
        }

        public string Logo { get; set; }

        public string Name { get; set; }

        public string City { get; set; }

        public string Province { get; set; }

        public string Country { get; set; }

        public ImageSource LogoImageSource {
            get {
                //TODO unfix to team
                return ImageSource.FromUri(new Uri($"{"xyz"}/sport/images/team/square/{Logo}"));
            }
        } 
    }
}
