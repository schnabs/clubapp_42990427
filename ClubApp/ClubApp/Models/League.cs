﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmHelpers;
using Newtonsoft.Json;

namespace ClubApp.Models
{
    public class League : DemoEntity
    {
        public League()
        {
            base.EntityID = (int)ClubEntities.League;

        }
            
        [JsonProperty(PropertyName = "abrv")]
        public string Abbreviation { get; set; }

        [JsonProperty(PropertyName = "sport_id")]
        public string SportID { get; set; }

        [JsonProperty(PropertyName = "association_id")]
        public string AssociationID { get; set; }

    }
}
