﻿using ClubApp.Data;
using ClubApp.Models;
using ClubApp.Views;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ClubApp.ViewModels
{
    class LeaguesViewModel : BaseViewModel
    {
        private ContentPage page;
        LeagueRestService leagueRestService;
        public ObservableRangeCollection<League> Leagues { get; set; } = new ObservableRangeCollection<League>();

        public LeaguesViewModel(ContentPage page)
        {
            this.page = page;
            Title = "Leagues Page";
            IsBusy = false;
            leagueRestService = new LeagueRestService();

        }

        private Command _UpdateLeagues;

        public ICommand UpdateLeagues
        {
            get
            {
                _UpdateLeagues = _UpdateLeagues ?? new Command(async () =>
                {
                    await LoadLeagues();
                }, () =>
                {
                    return !IsBusy;
                });
                return _UpdateLeagues;
            }
        }

        private async Task LoadLeagues()
        {
            IsBusy = true;
            _UpdateLeagues.ChangeCanExecute();
            Debug.WriteLine($"UpdateLeagues: Called GetLeaguesAsync");
            var leagues = await leagueRestService.GetLeaguesAsync();
            Leagues.ReplaceRange(leagues);
            IsBusy = false;
            _UpdateLeagues.ChangeCanExecute();
        }

        League _SelectedLeague;

        public League SelectedLeague
        {
            get { return _SelectedLeague; }
            set
            {
                _SelectedLeague = value;
                OnPropertyChanged();

                if (_SelectedLeague != null)
                {
                    Debug.WriteLine($"Navigating to ~ with LeagueID {_SelectedLeague.ID}");

                    //((MasterDetailPage)App.Current.MainPage).Detail = new NavigationPage(new DivisionsPage(_SelectedLeague));
                    ((MasterDetailPage)App.Current.MainPage).Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(DivisionsPage), new object[] { _SelectedLeague }));

                    _SelectedLeague = null;

                    //hide the Master page when an item is selected
                    //PD - this should be moved to the Master Detail code-behind page if possible
                    ((MasterDetailPage)App.Current.MainPage).IsPresented = false;
                }
            }
        }
    }
}
