﻿using ClubApp.Data;
using ClubApp.Models;
using ClubApp.Views;
using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ClubApp.ViewModels
{
    class DivisionsViewModel : BaseViewModel
    {
        private ContentPage page;
        DivisionsRestService divisionsRestService;

        public ObservableRangeCollection<Division> Divisions { set; get; } = new ObservableRangeCollection<Division>();

        private Command _UpdateDivisions;

        public ICommand UpdateDivisions
        {
            get
            {
                _UpdateDivisions = _UpdateDivisions ?? new Command(async () =>
                {
                    await LoadDivisions();
                }, () =>
                {
                    return !IsBusy;
                });
                return _UpdateDivisions;
            }
        }

        private async Task LoadDivisions()
        {
            IsBusy = true;
            _UpdateDivisions.ChangeCanExecute();
            Debug.WriteLine($"UpdatePoolRankings: Called GetDivisionsAsync");
            var divs = await divisionsRestService.GetDivisionsAsync(league);
            Divisions.ReplaceRange(divs);
            IsBusy = false;
            _UpdateDivisions.ChangeCanExecute();
        }

        Division selectedDivision;
        private League league;

        public Division SelectedDivision
        {
            get { return selectedDivision; }
            set
            {
                selectedDivision = value;
                OnPropertyChanged();

                if(selectedDivision != null)
                {
                    Debug.WriteLine($"Navigating to TeamRankingPage with DivisionID {selectedDivision.DivisionID}");
                    //page.Navigation.PushAsync(new PoolRankingsPage(selectedDivision));
                    selectedDivision = null;
                }
            }
        }

        public DivisionsViewModel(ContentPage page, League league)
        {
            this.page = page;
            this.league = league;
            Title = $"{league.Name} Divisions";
            IsBusy = false;

            divisionsRestService = new DivisionsRestService();         

        }

      
    }
}
