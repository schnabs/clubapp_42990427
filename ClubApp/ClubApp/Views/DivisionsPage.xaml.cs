﻿using ClubApp.Models;
using ClubApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClubApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DivisionsPage : ContentPage
    {
        DivisionsViewModel vm;

        public DivisionsPage(League league)
        {
            InitializeComponent();
            BindingContext = vm = new DivisionsViewModel(this, league);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            vm.UpdateDivisions.Execute(false);
        }

        //handle on back button pressed
        /*private bool _canClose = true;
        protected override bool OnBackButtonPressed()
        {
            if (_canClose)
            {
                ShowExitDialog();
            }
            return _canClose;
        }
        private async void ShowExitDialog()
        {
            var answer = await DisplayAlert("Exit", "Do you wan't to exit?", "Yes", "No");
            if (answer)
            {
                _canClose = false;
                base.OnBackButtonPressed();
            }
        }
        */
    }
}
