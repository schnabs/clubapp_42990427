﻿using ClubApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClubApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainMasterDetailPage : MasterDetailPage
    {
        public MainMasterDetailPage()
        {
            InitializeComponent();
            Master = new LeaguesPage();
            Detail = new NavigationPage(new DivisionsPage(new League()));

        }
        /*hiding the Master Page should eventually be moved to this file, not specifically in the Leagues View Model
         * void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            DisplayAlert("Test", "Item Selected", "Cool", "Beans");
            IsPresented = false;

        }
        */
    }
}
