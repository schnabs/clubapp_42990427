﻿using ClubApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ClubApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LeaguesPage : ContentPage
    {
        LeaguesViewModel vm;

        public LeaguesPage()
        {
            InitializeComponent();
            BindingContext = vm = new LeaguesViewModel(this);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            vm.UpdateLeagues.Execute(false);
        }
    }
}
