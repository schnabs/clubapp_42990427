﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClubApp
{
    class Constants
    {

        public static int AssociationID = 4;    //association ID for this Club
    }

    public enum ClubEntities : int { User = 1, Team = 2, League = 3, Association = 4, Venue=5, Event=6, Sport=9, Division = 11 };
}
